# Husky Example

This repository is meant to serve as an example for how to configure Husky on a project with the most useful hooks that I've used.

## Adding Husky

They just released version 7 of Husky, which goes about things quite differently from the way that it used to. Follow these steps to install Husky and my personal favorite hooks.

1. Navigate to your project folder's root directory
1. Run `npx husky-init && npm install`
1. Open the new `.husky/` directory that was just created and delete the default `pre-commit` file
1. Ensure that there is a `lint` script in your `package.json` file - configure it to use whatever external linter you want. In our case, we used ESLint because it is the default that comes with Create React App
1. Ensure that there is a `test` script in your `package.json` file - configure it to use whatever external testing suite you want. In our case, we used Jest because it is the default that comes with Create React App
1. Run `npx husky add .husky/pre-commit 'npm run lint'`
1. Run `npx husky add .husky/pre-push 'npm run test'`

You should be good to go! Now any time you run `git commit`, it will lint your files and stop the commit if something is amiss. Similarly, any time you run `git push` on your freshly linted commits, it will run your automated test suite. If any of the tests fail, it will automatically stop the push and display an error.

It's important to note that these things only stop the commits if your linter/test suite emits an exit code other than 0 when a lint or test fails. I've experienced some in the past that display errors but still return an exit code of 0, which was very confusing. :)

## Resources

- [Husky Docs](https://typicode.github.io/husky/#/?id=automatic-recommended)
- [List of Git Hooks](https://git-scm.com/docs/githooks)
- [ESLint Rules](https://eslint.org/docs/rules/)
- [Jest Docs](https://archive.jestjs.io/docs/en/22.x/getting-started.html)
